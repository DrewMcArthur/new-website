import * as React from "react";

import Portrait from "../assets/digital.jpg"

/**
 * Right panel.  Initial "Portrait" page
 */

export interface IPortraitProps { }

export default function IPortrait (props: IPortraitProps)
{
  return <div className="portrait">
    <a href="https://twitter.com/drewmca4MA">
      <img src={Portrait} alt="a digital portrait" />
    </a>
  </div>
}
