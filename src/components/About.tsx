import * as React from "react";

/**
 * Left panel.  Initial "About" page
 */

export interface IAboutProps { }

export default function IAbout (props: IAboutProps)
{
  return <div className="about">
    <h1>Campaign Site Coming Soon!</h1>
    <ul>
      <li>Write in Drew McArthur - 7 Sparhawk St #2, Boston MA!</li>
      <li>Running for Massachusetts State House Representative</li>
      <li>17th Suffolk District, Boston (Incumbent Kevin Honan)</li>
    </ul>
    <p>For updates, follow my campaign twitter <a href="https://twitter.com/drewmca4MA">@drewmca4MA</a></p>
  </div>
}
