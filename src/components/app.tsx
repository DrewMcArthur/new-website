import * as React from "react";

import About from './About'
import Portrait from './Portrait'

import '../style.css'

/**
 * Main app.  Handles overall CSS, 
 * including fade-in properties of the two main elements.
 */

export interface IAppProps { }

export default function IApp (props: IAppProps)
{
  return <div className="app">
    <About />
    <Portrait />
  </div>
}
